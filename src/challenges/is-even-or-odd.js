/*
  CHALLENGE:
    Dado un número entero devolver un texto "Es par" si es par y "Es impar" si es impar.
*/

/**
 * Función que verifica si el número recibido es un par o impar.
 * @param {number} num
 * @returns {string} "Es par" o "Es impar"
 */
export const isEvenOrOdd = (num) => {
  const checkCondition = (value) => Number(value) % 2 === 0 ? 'Es par' : 'Es impar';

  // isNaN lanza un error cuando se le pasa un BigInt, así que hacemos una primera comprobación:
  if (typeof num === 'bigint') return checkCondition(num);

  // Luego comprobamos los demás casos:
  if (typeof num !== 'number' || num === Infinity || isNaN(num)) return 'No es un número';

  // Todos los demás serán números:
  return checkCondition(num);
};
