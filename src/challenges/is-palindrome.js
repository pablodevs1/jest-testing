/*
  CHALLENGE:
    Dada una frase indicar si se trata de un palíndromo es decir, que se puede leer igual de derecha a izquierda que de izquierda a derecha,
    devolver un texto con el resultado de la verificación. "Es palíndromo" o "No es palíndromo".
*/

/**
 * Función que verifica si la cadena recibida es un palíndromo o no.
 * @param {string} phrase
 * @returns {string} "Es palíndromo" o "No es palíndromo"
 */
export const isPalindrome = phrase => {
  const phraseNormalized = phrase.toLowerCase().normalize('NFD') // transforma á -> a´
    .replace(/[\u0300-\u036f]/g, '') // Eliminamos las tildes
    .replace(/\s+/g, '') // Quitamos espacios
    .replace(/[,;.]/g, '') // Quitamos comas, puntos y punto y coma
    .split('');
  return phraseNormalized.join('') === phraseNormalized.reverse().join('') ? 'Es palíndromo' : 'No es palíndromo';
};
