/*
  CHALLENGE:
    Realiza una función que valide un NIF con las siguientes reglas:
      - El número y las letras tienen que formar siempre 9 caracteres
      - La letra debe ser correcta aplicando el conocido algoritmo (https://www.letranif.com/formula-para-calcular-nif/)
      - Devolver una estructura indicando el resultado de la verificación, y el motivo del error si lo hubiera.

*/

/**
 * Función que verifica si el nif recibido es correcto.
 * @param {string} nif
 * @returns {Object}
 */
export const validateNIF = (nif) => {
  const letters = 'TRWAGMYFPDXBNJZSQVHLCKET';
  const regExpNIF = /^[XYZ]?\d{5,8}[A-Z]$/;

  nif = nif.toUpperCase();

  if (regExpNIF.test(nif) === true && nif.length === 9) {

    const nifNumber = parseInt(
      nif.substring(0, nif.length)
        .replace('X', 0) // --> para números extranjeros
        .replace('Y', 1) // --> para números extranjeros
        .replace('Z', 2) // --> para números extranjeros
    );
    const nifLetter = nif[nif.length - 1];
    const expectedLetter = letters[nifNumber % 23];

    if (nifLetter === expectedLetter) {
      return {
        status: 'success',
      };
    }
    return {
      status: 'error',
      message: 'La letra del NIF no corresponde.',
    }
  }

  return {
    status: 'error',
    message: 'Formato no válido.'
  }
};