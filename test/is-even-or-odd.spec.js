import { isEvenOrOdd } from '../src/challenges/is-even-or-odd';

// Inicialización
const cases = [
  [2, 'Es par'],
  [-2, 'Es par'],
  [0, 'Es par'],
  [4.5, 'Es impar'],
  [789456131, 'Es impar'],
  [-123123, 'Es impar'],
  ['-123123', 'No es un número'],
  ['123123', 'No es un número'],
  ['0', 'No es un número'],
  [Infinity, 'No es un número'],
  [null, 'No es un número'],
  [undefined, 'No es un número'],
  [NaN, 'No es un número'],
  [11n, 'Es impar'],
  [0n, 'Es par'],
  [92233720368547758054n, 'Es par']
];

// Verificar todos
describe('isEvenOrOdd() utility', () => {
  test.each(cases)(
    'given %p as argument, returns %p',
    (firstArg, expectedResult) => {
      const result = isEvenOrOdd(firstArg);
      expect(result).toEqual(expectedResult);
    }
  );
});
