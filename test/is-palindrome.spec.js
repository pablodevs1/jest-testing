import { isPalindrome } from '../src/challenges/is-palindrome';

describe('Es palíndromo', () => {
  it('isPalindrome', () => {
    // Inicialización
    const firstCase = 'Yo dono rosas, o;ro no doy';
    const secondCase = 'Logra Casillas allí sacar gol';
    const thirdCase = 'Cualquier frase casi que se te ocurra';
    const fourthCase = 'La ruta nos aportó otro paso natural';
    const fifthCase = 'Allí por la tropa portado, traído a ese paraje de maniobras, una tipa como capitán usar boina me dejara, pese a odiar toda tropa por tal ropilla';

    // Verificar
    expect(isPalindrome(firstCase)).toEqual('Es palíndromo');
    expect(isPalindrome(secondCase)).toEqual('Es palíndromo');
    expect(isPalindrome(thirdCase)).toEqual('No es palíndromo');
    expect(isPalindrome(fourthCase)).toEqual('Es palíndromo');
    expect(isPalindrome(fifthCase)).toEqual('Es palíndromo');
  });
});
