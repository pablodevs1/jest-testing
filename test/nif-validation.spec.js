import { validateNIF } from '../src/challenges/nif-validation';

describe('isEvenOrOdd() utility', () => {
  it('validateNIF', () => {
    // Inicialización
    const firstCase = '98765432M';
    const secondCase = '45871236J';
    const thirdCase = '46879132L';
    const fourthCase = '46879132';
    const fifthCase = '1245678P';
    const sixthCase = 'CCCCCCCCC';
    const seventhCase = 'Y8946745N';

    // Verificar
    expect(validateNIF(firstCase).status).toEqual('success');
    expect(validateNIF(secondCase).status).toEqual('success');

    expect(validateNIF(thirdCase).status).toEqual('error');
    expect(validateNIF(thirdCase).message).toEqual('La letra del NIF no corresponde.');

    expect(validateNIF(fourthCase).status).toEqual('error');
    expect(validateNIF(fourthCase).message).toEqual('Formato no válido.');

    expect(validateNIF(fifthCase).status).toEqual('error');
    expect(validateNIF(fifthCase).message).toEqual('Formato no válido.');

    expect(validateNIF(sixthCase).status).toEqual('error');
    expect(validateNIF(sixthCase).message).toEqual('Formato no válido.');

    expect(validateNIF(seventhCase).status).toEqual('success');
  });
});
